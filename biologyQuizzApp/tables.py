import django_tables2 as tables
from .models import Image

class ImageTable(tables.Table):
    class Meta:
        model = Image

    Image = tables.TemplateColumn(
        '<a href="/static/images/{{ record.image_name }}.jpg"><img src="/static/images/{{ record.image_name }}.jpg" width="80" height="80"></a>')