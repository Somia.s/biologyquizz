from django.shortcuts import render
from django.contrib.auth import authenticate, login

from .models import Question, Answer, Image, Profile

from django.contrib.auth.forms import UserCreationForm # pour s'inscrire
from django.shortcuts import redirect

from .forms import FormAnswer # formulaire du quizz

import random

####################
##### SIGN UP ######
####################

def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    return render(request, 'registration/sign_up.html', {'form': form})





#################################
##### CONNECTED/DECONNECTED #####
#################################

def index(request):
    if request.user.is_authenticated:
        return render(request, "connected.html",
                      {"profile_user": Profile.objects.get(user_id=request.user.id),
                       "images": Image.objects.all().order_by('?')[0:24]})
    else:
        return render(request, "unconnected.html",
                      {"images": Image.objects.all().order_by('?')[0:24]})




#############################################
########### PLAY QUIZZ ######################
#############################################

def getImages(id_trueAnswer, choiceCategory):
    # choisir les bonnes images en fonction de la question posée et de la vraie réponse à trouver
    # id_trueAnswer: la vraie réponse que l'utilisateur doit cocher
    trueAnswer = Answer.objects.get(id=id_trueAnswer).answer
    nb_images = int(Question.objects.filter(category=choiceCategory).values()[0]["n_image"]) # cb d'images prendre
    if choiceCategory == "component":
        return Image.objects.filter(component=trueAnswer).order_by('?')[0:nb_images]
    elif choiceCategory == "microscopy":
        return Image.objects.filter(microscopy=trueAnswer).order_by('?')[0:nb_images]


class TrueAnswer:
    def __init__(self):
        self.IDtrueAnswer = None
    def setIDTrueAnswer(self, IDtrueAnswer):
        self.IDtrueAnswer = IDtrueAnswer
    def getIDTrueAnswer(self):
        return self.IDtrueAnswer
classTrueAnswer = TrueAnswer() # l'avoir en variable globale

class currentImages:
    def __init__(self):
        self.images = None
    def setImages(self, images):
        self.images = images
    def getImages(self):
        return self.images
classCurrentImages = currentImages() # l'avoir en variable globale


def playquizz(request, choiceCategory):
    # choiceCategory: le type de quizz
    # case of a classic quizz:
    choiceCategorySave = ["microscopy", "component", "classic"]
    del choiceCategorySave[choiceCategorySave.index(choiceCategory)]

    if choiceCategory == "classic":
        choiceCategory = random.choice(["microscopy", "component"])

    # Select the question of this category
    list_idQuestions = []
    id_questions = Question.objects.filter(category=choiceCategory)
    for i in range(len(id_questions)):
        list_idQuestions.append(id_questions.values()[i]["id"])
    questionIDcurrent = list_idQuestions[0] # une question ID = une seule catégorie, donc catégorie sous-entendu

    # Number of choices of answer to display
    nb_answer = int(Question.objects.filter(id=questionIDcurrent).values()[0]["n_answer"])

    #########################
    #### FORM TO RESPONSE ###
    #########################
    if request.method == "POST":
        form = FormAnswer(request.POST, questionID=questionIDcurrent, number_answer=nb_answer)

        # conserver les images actuelles:
        images = classCurrentImages.getImages()

        if form.is_valid(): #  procéder à la validation et renvoyer une valeur booléenne indiquant si les données sont valides
            id_answer_submitted = int(form.data['answer'])
            # Retrieve Profile DB of the gamer
            id_current_user = request.user.id # user id of the gamer
            profile_currently = Profile.objects.get(user_id=id_current_user)

            id_trueAnswer = classTrueAnswer.getIDTrueAnswer()

            if id_trueAnswer == id_answer_submitted: # User responde correctly
                profile_currently.total_score += Question.objects.get(category=choiceCategory).points # change field
                profile_currently.save()  # this will update only
                if choiceCategory == "component":
                    profile_currently.component_score += Question.objects.get(category=choiceCategory).points # change field
                    profile_currently.save()  # this will update only
                elif choiceCategory == "microscopy":
                    profile_currently.microscopy_score += Question.objects.get(category=choiceCategory).points # change field
                    profile_currently.save()  # this will update only
                user_answer = True

            else: # User is wrong
                profile_currently.total_score -= Question.objects.get(category=choiceCategory).points # change field
                profile_currently.save()  # this will update only
                if choiceCategory == "component":
                    profile_currently.component_score -= Question.objects.get(category=choiceCategory).points # change field
                    profile_currently.save()  # this will update only
                elif choiceCategory == "microscopy":
                    profile_currently.microscopy_score -= Question.objects.get(category=choiceCategory).points # change field
                    profile_currently.save()  # this will update only
                user_answer = False

            # change level of gamer
            if int(profile_currently.total_score) < 50:
                profile_currently.level = "beginner"
            if 50 <= int(profile_currently.total_score) < 100:
                profile_currently.level = "medium"
            elif int(profile_currently.total_score) >= 100:
                profile_currently.level = "advanced"
            profile_currently.save()

            # New form for the following question
            form = FormAnswer(questionID=questionIDcurrent, number_answer=nb_answer)  # on fait d'abord le formulaire
            trueAnswer = form.returnTrueAnswer()
            id_trueAnswer = Answer.objects.get(answer=trueAnswer).id

            classTrueAnswer.setIDTrueAnswer(id_trueAnswer)

            return render(request, "playquizz2.html",
                                              {"profile_user": Profile.objects.get(user_id=request.user.id),
                                               "questions": Question.objects.filter(category=choiceCategory, id=questionIDcurrent),
                                               "images": getImages(id_trueAnswer, choiceCategory),
                                               'form': form,
                                               'successful_submit': True,
                                               'user_answer': user_answer,
                                               'true_answer': Answer.objects.get(id=id_trueAnswer).answer,
                                               'otherCategories': choiceCategorySave,
                                               'category_currently': choiceCategory,
                                               'images_currently': images})
    # pour la 1ère soumission:
    else:
        # 1ère formulaire
        form = FormAnswer(questionID=questionIDcurrent, number_answer=nb_answer) # on fait d'abord le formulaire
        trueAnswer = form.returnTrueAnswer()
        id_trueAnswer = Answer.objects.get(answer=trueAnswer).id
        classTrueAnswer.setIDTrueAnswer(id_trueAnswer)

        # save currently images
        images = getImages(id_trueAnswer, choiceCategory)
        classCurrentImages.setImages(images)

        return render(request, "playquizz.html",
                                  {"profile_user": Profile.objects.get(user_id=request.user.id),
                                   "questions": Question.objects.filter(category=choiceCategory, id=questionIDcurrent),
                                   "images": images,
                                   'form': form,
                                   'true_answer': Answer.objects.get(id=id_trueAnswer).answer,
                                   'otherCategories': choiceCategorySave,
                                   'category_currently': choiceCategory})








########################################
########## Filtrer les images  #########
########## table               #########
########################################

from django_tables2.views import SingleTableMixin
from django_filters.views import FilterView
from .tables import ImageTable
from biologyQuizzApp.filters import ImageFilter

class FilteredImagesListView(SingleTableMixin, FilterView):

    table_class = ImageTable
    model = Image
    template_name = 'explorepages.html'
    filterset_class = ImageFilter


#######################################################
##### Edit Image DB directly in admin interface #######
#######################################################
from dal import autocomplete
class ImagesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Image.objects.none()
        qs = Image.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

