# biologyQuizz

# Step 0: Run quizz

# Locally
conda activate djangoenv

cd biologyQuizz

python manage.py runserver


# Remote server
https://biologyquizz2020.herokuapp.com/

# Step 1: Sign up or Login

# Step 2: PLAY QUIZZ

According to the question category, random selection of an answer.

Then random selection of a quantity corresponding to the image answer.


Score informations at the header.

Increment or decrement of score after each submit in the quizz.

Score in real terms visible, and level of the gamer at the footer.


# Step 3: EXPLORES PAGES

You can filter images according to their features.
With pagination at bottom.

# ABOUT MODULES USED

## Header and Footer
MDBootstrap.com

## Form
forms.ModelMultipleChoiceFields

## Page to explore images

table: django-tables2 and Bootstrap 3

tablesorter for search values: django-filter

autocompletion: ModelChoiceFilter of danjgo-filter


## Miscellaneous

Edit directly models in Admin Interface of Django: django-autocomplete-light, dal and dal_select2
