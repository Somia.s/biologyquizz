import csv
from .models import Question, Answer, Image
from django.http import HttpResponse

def importDatas(request):
    # CSV file path name
    csv_questions = "./data/questions.csv"
    csv_answers = "./data/answers.csv"
    csv_images = "./data/images.csv"

    # Read files
    dataReaderQ = csv.reader(open(csv_questions), delimiter='\t', quotechar='"')
    dataReaderA = csv.reader(open(csv_answers), delimiter='\t', quotechar='"')
    dataReaderI = csv.reader(open(csv_images), delimiter='\t', quotechar='"')

    # Remove the first line (header)
    next(dataReaderQ)
    next(dataReaderA)
    next(dataReaderI)

    # Parse files
    for row in dataReaderQ:
        question=Question()
        question.question = row[1]
        question.category = row[2]
        question.imageField = row[3]
        question.points = row[4]
        question.n_answer = row[5]
        question.n_image = row[6]
        question.save()

    for row in dataReaderA:
        answer=Answer()
        answer.question_id = row[1]
        answer.answer = row[2]
        answer.definition = row[3]
        answer.save()

    for row in dataReaderI:
        image=Image()
        image.image_name = row[1]
        image.description = row[2]
        image.microscopy = row[3]
        image.celltype = row[4]
        image.component = row[5]
        image.doi = row[6]
        image.organism = row[7]
        image.save()

    return HttpResponse("<p>Image, Question and Answer imported.</p>")