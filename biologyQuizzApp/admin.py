from django.contrib import admin
from .models import Image, Answer, Question
from .forms import FormImage, FormAnswerForAdminInterface, FormQuestion

# Register your models here.

#######################################################
##### Edit Image DB directly in admin interface #######
#######################################################
class PersonAdmin(admin.ModelAdmin):
    form = FormImage
admin.site.register(Image, PersonAdmin)

#######################################################
##### Edit Answer DB directly in admin interface ######
#######################################################
class PersonAdmin(admin.ModelAdmin):
    form = FormAnswerForAdminInterface
admin.site.register(Answer, PersonAdmin)

#######################################################
##### Edit Question DB directly in admin interface ####
#######################################################
class PersonAdmin(admin.ModelAdmin):
    form = FormQuestion
admin.site.register(Question, PersonAdmin)