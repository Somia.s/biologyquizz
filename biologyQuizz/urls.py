"""biologyQuizz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from biologyQuizzApp.views import index, sign_up, playquizz, FilteredImagesListView
from django.contrib.auth.views import LoginView, LogoutView
from biologyQuizzApp.importDatas import importDatas
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls.static import static

# Edit Image DB directly in admin interface
from biologyQuizzApp.models import Image, Answer, Question
from django.conf.urls import url
from dal import autocomplete


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('sign_up/', sign_up, name="sign-up"),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('importdatas/', importDatas),
    path('quizz/<choiceCategory>/', playquizz, name='playquizz'),
    path('explorepages/', FilteredImagesListView.as_view(), name='explorepages'),
    url( # Register the autocomplete view
        'images-autocomplete/$',
        autocomplete.Select2QuerySetView.as_view(model=Image),
        name='images-autocomplete',
    ),
    url(  # Register the autocomplete view
        'answers-autocomplete/$',
        autocomplete.Select2QuerySetView.as_view(model=Answer),
        name='answers-autocomplete',
    ),
    url(  # Register the autocomplete view
        'questions-autocomplete/$',
        autocomplete.Select2QuerySetView.as_view(model=Question),
        name='questions-autocomplete',
    ),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)