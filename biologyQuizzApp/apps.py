from django.apps import AppConfig


class BiologyquizzappConfig(AppConfig):
    name = 'biologyQuizzApp'
