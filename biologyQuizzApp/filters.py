from django_filters import FilterSet, ModelChoiceFilter
from dal import autocomplete
from .models import Image

class ImageFilter(FilterSet):

    # autocompletion
    image_name = ModelChoiceFilter(queryset=Image.objects.values_list('image_name', flat=True).distinct())
    microscopy = ModelChoiceFilter(queryset=Image.objects.values_list('microscopy', flat=True).distinct())

    class Meta:
        model = Image
        fields = ['id', 'image_name', 'description', 'microscopy', 'celltype', 'component', 'doi', 'organism']